/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/js/Footer.js":
/*!**************************!*\
  !*** ./src/js/Footer.js ***!
  \**************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _Global__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Global */ \"./src/js/Global.js\");\n\n\nvar Footer = function Footer() {\n  window.innerWidth <= 768 && (0,_Global__WEBPACK_IMPORTED_MODULE_0__.accordion)({\n    title: '.footer .accordion__title',\n    content: '.footer .accordion__content'\n  });\n};\n\nFooter();//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvanMvRm9vdGVyLmpzLmpzIiwibWFwcGluZ3MiOiI7O0FBQUE7O0FBRUEsSUFBTUMsTUFBTSxHQUFHLFNBQVRBLE1BQVMsR0FBTTtBQUNqQkMsRUFBQUEsTUFBTSxDQUFDQyxVQUFQLElBQXFCLEdBQXJCLElBQTRCSCxrREFBUyxDQUFDO0FBQUNJLElBQUFBLEtBQUssRUFBRSwyQkFBUjtBQUFxQ0MsSUFBQUEsT0FBTyxFQUFFO0FBQTlDLEdBQUQsQ0FBckM7QUFDSCxDQUZEOztBQUlBSixNQUFNIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vQHdlYXJlYXRobG9uL2Zyb250ZW5kLXdlYnBhY2stYm9pbGVycGxhdGUvLi9zcmMvanMvRm9vdGVyLmpzPzFlZDEiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgYWNjb3JkaW9uIH0gZnJvbSBcIi4vR2xvYmFsXCI7XG5cbmNvbnN0IEZvb3RlciA9ICgpID0+IHtcbiAgICB3aW5kb3cuaW5uZXJXaWR0aCA8PSA3NjggJiYgYWNjb3JkaW9uKHt0aXRsZTogJy5mb290ZXIgLmFjY29yZGlvbl9fdGl0bGUnLCBjb250ZW50OiAnLmZvb3RlciAuYWNjb3JkaW9uX19jb250ZW50J30pO1xufTtcblxuRm9vdGVyKCk7XG4iXSwibmFtZXMiOlsiYWNjb3JkaW9uIiwiRm9vdGVyIiwid2luZG93IiwiaW5uZXJXaWR0aCIsInRpdGxlIiwiY29udGVudCJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/js/Footer.js\n");

/***/ }),

/***/ "./src/js/Global.js":
/*!**************************!*\
  !*** ./src/js/Global.js ***!
  \**************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"accordion\": () => (/* binding */ accordion)\n/* harmony export */ });\nvar accordion = function accordion(_ref) {\n  var title = _ref.title,\n      content = _ref.content;\n  var acc = document.querySelectorAll(title);\n  var allPanel = document.querySelectorAll(content);\n  acc.forEach(function (element) {\n    if (element.classList.contains(\"active\")) {\n      var panel = element.nextElementSibling;\n      panel.style.maxHeight = panel.scrollHeight + \"px\";\n    }\n\n    element.addEventListener(\"click\", function () {\n      var panel = this.nextElementSibling;\n\n      if (this.classList.contains(\"active\")) {\n        this.classList.remove(\"active\");\n        panel.style.maxHeight = null;\n        panel.classList.remove(\"active\");\n      } else {\n        allPanel.forEach(function (remove) {\n          if (remove.previousElementSibling.classList.contains(\"active\")) {\n            remove.previousElementSibling.classList.remove(\"active\");\n            remove.classList.remove(\"active\");\n            remove.style.maxHeight = null;\n          }\n        });\n        this.classList.add(\"active\");\n\n        if (panel.style.maxHeight) {\n          panel.style.maxHeight = null;\n        } else {\n          panel.classList.add(\"active\");\n          panel.style.maxHeight = panel.scrollHeight + \"px\";\n        }\n      }\n    });\n  });\n};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvanMvR2xvYmFsLmpzLmpzIiwibWFwcGluZ3MiOiI7Ozs7QUFBTyxJQUFNQSxTQUFTLEdBQUcsU0FBWkEsU0FBWSxPQUFzQjtBQUFBLE1BQXBCQyxLQUFvQixRQUFwQkEsS0FBb0I7QUFBQSxNQUFiQyxPQUFhLFFBQWJBLE9BQWE7QUFDM0MsTUFBSUMsR0FBRyxHQUFHQyxRQUFRLENBQUNDLGdCQUFULENBQTBCSixLQUExQixDQUFWO0FBQ0EsTUFBSUssUUFBUSxHQUFHRixRQUFRLENBQUNDLGdCQUFULENBQTBCSCxPQUExQixDQUFmO0FBQ0FDLEVBQUFBLEdBQUcsQ0FBQ0ksT0FBSixDQUFZLFVBQUFDLE9BQU8sRUFBSTtBQUNuQixRQUFJQSxPQUFPLENBQUNDLFNBQVIsQ0FBa0JDLFFBQWxCLENBQTJCLFFBQTNCLENBQUosRUFBMEM7QUFDdEMsVUFBSUMsS0FBSyxHQUFHSCxPQUFPLENBQUNJLGtCQUFwQjtBQUNBRCxNQUFBQSxLQUFLLENBQUNFLEtBQU4sQ0FBWUMsU0FBWixHQUF3QkgsS0FBSyxDQUFDSSxZQUFOLEdBQXFCLElBQTdDO0FBQ0g7O0FBQ0RQLElBQUFBLE9BQU8sQ0FBQ1EsZ0JBQVIsQ0FBeUIsT0FBekIsRUFBa0MsWUFBWTtBQUMxQyxVQUFJTCxLQUFLLEdBQUcsS0FBS0Msa0JBQWpCOztBQUNBLFVBQUksS0FBS0gsU0FBTCxDQUFlQyxRQUFmLENBQXdCLFFBQXhCLENBQUosRUFBdUM7QUFDbkMsYUFBS0QsU0FBTCxDQUFlUSxNQUFmLENBQXNCLFFBQXRCO0FBQ0FOLFFBQUFBLEtBQUssQ0FBQ0UsS0FBTixDQUFZQyxTQUFaLEdBQXdCLElBQXhCO0FBQ0FILFFBQUFBLEtBQUssQ0FBQ0YsU0FBTixDQUFnQlEsTUFBaEIsQ0FBdUIsUUFBdkI7QUFDSCxPQUpELE1BSU87QUFDSFgsUUFBQUEsUUFBUSxDQUFDQyxPQUFULENBQWlCLFVBQUFVLE1BQU0sRUFBSTtBQUN2QixjQUFJQSxNQUFNLENBQUNDLHNCQUFQLENBQThCVCxTQUE5QixDQUF3Q0MsUUFBeEMsQ0FBaUQsUUFBakQsQ0FBSixFQUFnRTtBQUM1RE8sWUFBQUEsTUFBTSxDQUFDQyxzQkFBUCxDQUE4QlQsU0FBOUIsQ0FBd0NRLE1BQXhDLENBQStDLFFBQS9DO0FBQ0FBLFlBQUFBLE1BQU0sQ0FBQ1IsU0FBUCxDQUFpQlEsTUFBakIsQ0FBd0IsUUFBeEI7QUFDQUEsWUFBQUEsTUFBTSxDQUFDSixLQUFQLENBQWFDLFNBQWIsR0FBeUIsSUFBekI7QUFDSDtBQUNKLFNBTkQ7QUFPQSxhQUFLTCxTQUFMLENBQWVVLEdBQWYsQ0FBbUIsUUFBbkI7O0FBQ0EsWUFBSVIsS0FBSyxDQUFDRSxLQUFOLENBQVlDLFNBQWhCLEVBQTJCO0FBQ3ZCSCxVQUFBQSxLQUFLLENBQUNFLEtBQU4sQ0FBWUMsU0FBWixHQUF3QixJQUF4QjtBQUNILFNBRkQsTUFFTztBQUNISCxVQUFBQSxLQUFLLENBQUNGLFNBQU4sQ0FBZ0JVLEdBQWhCLENBQW9CLFFBQXBCO0FBQ0FSLFVBQUFBLEtBQUssQ0FBQ0UsS0FBTixDQUFZQyxTQUFaLEdBQXdCSCxLQUFLLENBQUNJLFlBQU4sR0FBcUIsSUFBN0M7QUFDSDtBQUNKO0FBQ0osS0F0QkQ7QUF1QkgsR0E1QkQ7QUE2QkgsQ0FoQ00iLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9Ad2VhcmVhdGhsb24vZnJvbnRlbmQtd2VicGFjay1ib2lsZXJwbGF0ZS8uL3NyYy9qcy9HbG9iYWwuanM/YTVjOSJdLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY29uc3QgYWNjb3JkaW9uID0gKHt0aXRsZSwgY29udGVudH0pID0+IHtcclxuICAgIGxldCBhY2MgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKHRpdGxlKTtcclxuICAgIGxldCBhbGxQYW5lbCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoY29udGVudClcclxuICAgIGFjYy5mb3JFYWNoKGVsZW1lbnQgPT4ge1xyXG4gICAgICAgIGlmIChlbGVtZW50LmNsYXNzTGlzdC5jb250YWlucyhcImFjdGl2ZVwiKSkge1xyXG4gICAgICAgICAgICBsZXQgcGFuZWwgPSBlbGVtZW50Lm5leHRFbGVtZW50U2libGluZztcclxuICAgICAgICAgICAgcGFuZWwuc3R5bGUubWF4SGVpZ2h0ID0gcGFuZWwuc2Nyb2xsSGVpZ2h0ICsgXCJweFwiO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIGxldCBwYW5lbCA9IHRoaXMubmV4dEVsZW1lbnRTaWJsaW5nO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5jbGFzc0xpc3QuY29udGFpbnMoXCJhY3RpdmVcIikpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY2xhc3NMaXN0LnJlbW92ZShcImFjdGl2ZVwiKTtcclxuICAgICAgICAgICAgICAgIHBhbmVsLnN0eWxlLm1heEhlaWdodCA9IG51bGw7XHJcbiAgICAgICAgICAgICAgICBwYW5lbC5jbGFzc0xpc3QucmVtb3ZlKFwiYWN0aXZlXCIpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgYWxsUGFuZWwuZm9yRWFjaChyZW1vdmUgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZW1vdmUucHJldmlvdXNFbGVtZW50U2libGluZy5jbGFzc0xpc3QuY29udGFpbnMoXCJhY3RpdmVcIikpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVtb3ZlLnByZXZpb3VzRWxlbWVudFNpYmxpbmcuY2xhc3NMaXN0LnJlbW92ZShcImFjdGl2ZVwiKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVtb3ZlLmNsYXNzTGlzdC5yZW1vdmUoXCJhY3RpdmVcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlbW92ZS5zdHlsZS5tYXhIZWlnaHQgPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICB0aGlzLmNsYXNzTGlzdC5hZGQoXCJhY3RpdmVcIik7XHJcbiAgICAgICAgICAgICAgICBpZiAocGFuZWwuc3R5bGUubWF4SGVpZ2h0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcGFuZWwuc3R5bGUubWF4SGVpZ2h0ID0gbnVsbDtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcGFuZWwuY2xhc3NMaXN0LmFkZChcImFjdGl2ZVwiKTtcclxuICAgICAgICAgICAgICAgICAgICBwYW5lbC5zdHlsZS5tYXhIZWlnaHQgPSBwYW5lbC5zY3JvbGxIZWlnaHQgKyBcInB4XCI7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH0pXHJcbn0iXSwibmFtZXMiOlsiYWNjb3JkaW9uIiwidGl0bGUiLCJjb250ZW50IiwiYWNjIiwiZG9jdW1lbnQiLCJxdWVyeVNlbGVjdG9yQWxsIiwiYWxsUGFuZWwiLCJmb3JFYWNoIiwiZWxlbWVudCIsImNsYXNzTGlzdCIsImNvbnRhaW5zIiwicGFuZWwiLCJuZXh0RWxlbWVudFNpYmxpbmciLCJzdHlsZSIsIm1heEhlaWdodCIsInNjcm9sbEhlaWdodCIsImFkZEV2ZW50TGlzdGVuZXIiLCJyZW1vdmUiLCJwcmV2aW91c0VsZW1lbnRTaWJsaW5nIiwiYWRkIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/js/Global.js\n");

/***/ }),

/***/ "./src/js/JobPage.js":
/*!***************************!*\
  !*** ./src/js/JobPage.js ***!
  \***************************/
/***/ (() => {

eval("//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsInNvdXJjZXNDb250ZW50IjpbIiJdLCJmaWxlIjoiLi9zcmMvanMvSm9iUGFnZS5qcy5qcyIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/js/JobPage.js\n");

/***/ }),

/***/ "./src/js/app.js":
/*!***********************!*\
  !*** ./src/js/app.js ***!
  \***********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _scss_app_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../scss/app.scss */ \"./src/scss/app.scss\");\n/* harmony import */ var _Global_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Global.js */ \"./src/js/Global.js\");\n/* harmony import */ var _JobPage_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./JobPage.js */ \"./src/js/JobPage.js\");\n/* harmony import */ var _JobPage_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_JobPage_js__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _Footer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Footer.js */ \"./src/js/Footer.js\");\n\n/* Your JS Code goes here */\n\n\n/* page JS */\n\n\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvanMvYXBwLmpzLmpzIiwibWFwcGluZ3MiOiI7Ozs7OztBQUFBO0FBRUE7O0FBQ0E7QUFFQTs7QUFDQSIsInNvdXJjZXMiOlsid2VicGFjazovL0B3ZWFyZWF0aGxvbi9mcm9udGVuZC13ZWJwYWNrLWJvaWxlcnBsYXRlLy4vc3JjL2pzL2FwcC5qcz85MGU5Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAnLi4vc2Nzcy9hcHAuc2Nzcyc7XG5cbi8qIFlvdXIgSlMgQ29kZSBnb2VzIGhlcmUgKi9cbmltcG9ydCAnLi9HbG9iYWwuanMnO1xuXG4vKiBwYWdlIEpTICovXG5pbXBvcnQgJy4vSm9iUGFnZS5qcyc7XG5pbXBvcnQgJy4vRm9vdGVyLmpzJztcbiJdLCJuYW1lcyI6W10sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/js/app.js\n");

/***/ }),

/***/ "./src/scss/app.scss":
/*!***************************!*\
  !*** ./src/scss/app.scss ***!
  \***************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n// extracted by mini-css-extract-plugin\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvc2Nzcy9hcHAuc2Nzcy5qcyIsIm1hcHBpbmdzIjoiO0FBQUEiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9Ad2VhcmVhdGhsb24vZnJvbnRlbmQtd2VicGFjay1ib2lsZXJwbGF0ZS8uL3NyYy9zY3NzL2FwcC5zY3NzPzYyOWUiXSwic291cmNlc0NvbnRlbnQiOlsiLy8gZXh0cmFjdGVkIGJ5IG1pbmktY3NzLWV4dHJhY3QtcGx1Z2luXG5leHBvcnQge307Il0sIm5hbWVzIjpbXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/scss/app.scss\n");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval-source-map devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./src/js/app.js");
/******/ 	
/******/ })()
;