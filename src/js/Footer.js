import { accordion } from "./Global";

const Footer = () => {
    window.innerWidth <= 768 && accordion({title: '.footer .accordion__title', content: '.footer .accordion__content'});
};

Footer();
