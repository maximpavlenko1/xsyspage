export const accordion = ({title, content}) => {
    let acc = document.querySelectorAll(title);
    let allPanel = document.querySelectorAll(content)
    acc.forEach(element => {
        if (element.classList.contains("active")) {
            let panel = element.nextElementSibling;
            panel.style.maxHeight = panel.scrollHeight + "px";
        }
        element.addEventListener("click", function () {
            let panel = this.nextElementSibling;
            if (this.classList.contains("active")) {
                this.classList.remove("active");
                panel.style.maxHeight = null;
                panel.classList.remove("active");
            } else {
                allPanel.forEach(remove => {
                    if (remove.previousElementSibling.classList.contains("active")) {
                        remove.previousElementSibling.classList.remove("active");
                        remove.classList.remove("active");
                        remove.style.maxHeight = null;
                    }
                })
                this.classList.add("active");
                if (panel.style.maxHeight) {
                    panel.style.maxHeight = null;
                } else {
                    panel.classList.add("active");
                    panel.style.maxHeight = panel.scrollHeight + "px";
                }
            }
        });
    })
}